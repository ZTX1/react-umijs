
import React from 'react';
import Link from 'umi/link';
import styles from './about.css';

export default function() {
  return (
    <div className="mt-5">
      <h1>Page about</h1>
      <p>Passionné par les nouvelles technologies, je me suis dirigé vers le développement de site internet, incontournable à notre époque.
Je suis également intéressé par tout les domaines autours comme le graphisme avec les différents choix à faire pour améliorer l'expérience utilisateur ou l'étude commerciale pour comprendre les besoins et attentes d'un logiciel.</p>
      <p>Vous trouverez plus d'information sur mon <a href="https://www.linkedin.com/in/benjamin-mallinus-15b25a132/">linkedin</a></p>
    </div>
  );
}
