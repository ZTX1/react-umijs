import React from 'react';
import styles from './index.css';

export default class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  route() {
    if(this.state.value == '')
      return "/hello"
    return "/hello/" + this.state.value;
  }

  render() {
    return (
      <div className="mt-5">
        <h1>Entrez un nom pour dire bonjour</h1>
        <form action={this.route()} method="POST">
          <label>
            Nom :
            <input type="text" value={this.state.value} onChange={this.handleChange} />
          </label>
          <input type="submit" value="Envoyer" />
        </form>
      </div>
    );
  }
}