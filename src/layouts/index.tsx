import React from 'react';
import Link from 'umi/link';

const BasicLayout: React.FC = props => {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="collapse navbar-collapse">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item"><Link className="nav-link" to="/">Home</Link></li>
          <li className="nav-item"><Link className="nav-link" to="/about">About</Link></li>
        </ul>
      </div>
        
      </nav>
      <div className="container">{props.children}</div>
    </div>
    
  );
};

export default BasicLayout;
